package com.example.consumer.util;

public class Constant {
	public static final String PRODUCT_CLASS_PATH = "com.example.consumer.dto.Product";
	public static final String PACKAGES_TO_TRUST = "*";

	public static final String GROUP_ID_VALUE = "group_id";

	public static final String TOPIC_NAME_1 = "topic1";
}
