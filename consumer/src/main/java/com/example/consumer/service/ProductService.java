package com.example.consumer.service;

import org.springframework.stereotype.Service;

import com.example.consumer.dto.Product;
import com.example.consumer.repository.ProductRepository;

@Service
public class ProductService {

	private final ProductRepository productRepository;

	public ProductService(final ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	public Product saveMessage(final Product product) {
		return productRepository.save(product);
	}
}
