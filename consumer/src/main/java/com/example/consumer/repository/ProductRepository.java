package com.example.consumer.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.consumer.dto.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, String> {
}
