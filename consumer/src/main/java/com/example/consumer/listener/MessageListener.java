package com.example.consumer.listener;

import static com.example.consumer.util.Constant.GROUP_ID_VALUE;
import static com.example.consumer.util.Constant.TOPIC_NAME_1;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.example.consumer.dto.Product;
import com.example.consumer.service.ProductService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class MessageListener {

	private final ProductService dataService;

	public MessageListener(final ProductService dataService) {
		this.dataService = dataService;
	}

	@KafkaListener(groupId = GROUP_ID_VALUE, topics = TOPIC_NAME_1, containerFactory = "kafkaListenerContainerFactory")
	public void listener(final Product product) {
		log.info("Recieved message: {}", product);
		dataService.saveMessage(product);
	}
}
