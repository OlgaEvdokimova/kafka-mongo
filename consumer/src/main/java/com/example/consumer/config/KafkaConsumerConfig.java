package com.example.consumer.config;

import static com.example.consumer.util.Constant.GROUP_ID_VALUE;
import static com.example.consumer.util.Constant.PACKAGES_TO_TRUST;
import static com.example.consumer.util.Constant.PRODUCT_CLASS_PATH;

import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.mapping.DefaultJackson2JavaTypeMapper;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import com.example.consumer.dto.Product;
import com.example.consumer.properties.KafkaConsumerProperties;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

	private final KafkaConsumerProperties kafkaConsumerProperties;

	public KafkaConsumerConfig(final KafkaConsumerProperties kafkaConsumerProperties) {
		this.kafkaConsumerProperties = kafkaConsumerProperties;
	}

	public ConsumerFactory<String, Product> consumerFactory() {
		Map<String, Object> config = Map.of(
				ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConsumerProperties.getBootstrapAddress(),
				ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID_VALUE
		);

		DefaultJackson2JavaTypeMapper typeMapper = new DefaultJackson2JavaTypeMapper();
		typeMapper.setIdClassMapping(Map.of(PRODUCT_CLASS_PATH, Product.class));
		typeMapper.addTrustedPackages(PACKAGES_TO_TRUST);

		JsonDeserializer<Product> jsonDeserializer = new JsonDeserializer<>(Product.class);
		jsonDeserializer.setTypeMapper(typeMapper);
		jsonDeserializer.setUseTypeMapperForKey(true);

		return new DefaultKafkaConsumerFactory<>(config, new StringDeserializer(), jsonDeserializer);
	}

	@Bean
	public ConcurrentKafkaListenerContainerFactory<String, Product> kafkaListenerContainerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, Product> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(consumerFactory());
		return factory;
	}
}
