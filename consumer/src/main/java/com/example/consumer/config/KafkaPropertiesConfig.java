package com.example.consumer.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.consumer.properties.KafkaConsumerProperties;

@Configuration
public class KafkaPropertiesConfig {

	@Bean
	@ConfigurationProperties(prefix = "kafka.props")
	public KafkaConsumerProperties getKafkaProperties() {
		return new KafkaConsumerProperties();
	}
}
