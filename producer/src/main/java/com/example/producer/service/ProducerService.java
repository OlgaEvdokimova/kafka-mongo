package com.example.producer.service;

import java.util.UUID;

import org.jeasy.random.EasyRandom;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.example.producer.dto.Product;
import com.example.producer.util.Constant;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProducerService {

	private final KafkaTemplate<String, Product> kafkaTemplate;

	public ProducerService(final KafkaTemplate<String, Product> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	public void generate(final Product product) {
		ListenableFuture<SendResult<String, Product>> future = kafkaTemplate.send(Constant.TOPIC_NAME_1, UUID.randomUUID().toString(), product);

		future.addCallback(new ListenableFutureCallback<>() {
			@Override
			public void onFailure(final Throwable throwable) {
				log.error("Unable to send message = {} dut to: {}", product, throwable.getMessage());
			}

			@Override
			public void onSuccess(final SendResult<String, Product> stringDataSendResult) {
				log.info("Sent Message = {} with offset = {}", product, stringDataSendResult.getRecordMetadata().offset());
			}
		});
	}

	public void generate(final int count) {

		EasyRandom generator = new EasyRandom();

		for (int i = 1; i < count; i++) {

			Product generatedProduct = generator.nextObject(Product.class);
			ListenableFuture<SendResult<String, Product>> future = kafkaTemplate.send(Constant.TOPIC_NAME_1, generatedProduct);

			future.addCallback(new ListenableFutureCallback<>() {
				@Override
				public void onFailure(final Throwable throwable) {
					log.error("Unable to send message = {} dut to: {}", generatedProduct, throwable.getMessage());
				}

				@Override
				public void onSuccess(final SendResult<String, Product> stringDataSendResult) {
					log.info("Sent Generated Message = {} with offset = {}", generatedProduct,
							stringDataSendResult.getRecordMetadata().offset());
				}
			});
		}
	}
}
