package com.example.producer.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.producer.dto.Product;
import com.example.producer.service.ProducerService;

@RestController
@RequestMapping
public class ProducerController {

	private final ProducerService producerService;

	public ProducerController(final ProducerService producerService) {
		this.producerService = producerService;
	}

	@PostMapping("/store")
	public Product generate(@RequestBody final Product product) {
		producerService.generate(product);
		return product;
	}

	@PostMapping("/generate/{count}")
	public void generateProduct(@PathVariable final int count) {
		producerService.generate(count);
	}
}
